use nalgebra::base::{DMatrix as DM, DVector as DV, Vector as V};

use nalgebra::base::{dimension::Dim, dimension::Dynamic, storage::Storage, Matrix, Scalar, U1};


pub trait VectorFn {
    fn apply<D: Dim, S: Storage<f64, D>>(&self, x: &V<f64, D, S>) -> f64;
}

pub struct NormSquare;

impl VectorFn for NormSquare {
    fn apply<D: Dim, S: Storage<f64, D>>(&self, x: &V<f64, D, S>) -> f64 {
        norm_square(x)
    }
}
fn norm_square<D: Dim, S: Storage<f64, D>>(x: &V<f64, D, S>) -> f64 {
    x.iter().fold(0., |b, xi| b + xi.powi(2))
}


// fn sphere<D: Dim, S: Storage<f64, D>>(x: &V<f64, D, S>) -> f64 {
//     x.iter().fold(0., |b, xi| b + xi.powi(2))
// }
