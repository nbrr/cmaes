use nalgebra::base::{DMatrix, DVector, Vector};

use nalgebra::base::{dimension::Dim, dimension::Dynamic, storage::Storage, U1};

use chrono;
use fern;
use log::*;

use cma::*;
use cma::ff::*;


fn setup_logger() -> Result<(), fern::InitError> {
    fern::Dispatch::new()
        .format(|out, message, record| out.finish(format_args!("[{}] {}", record.level(), message)))
        .level(log::LevelFilter::Trace)
        .chain(std::io::stdout())
        .chain(fern::log_file(format!(
            "logs/{}-cma-output.log",
            chrono::Local::now().format("[%Y-%m-%d][%H:%M:%S]")
        ))?)
        .apply()?;
    Ok(())
}

fn main() {
    setup_logger();
    cma(NormSquare, 2., 100., 20000);
}
