use nalgebra::base::{DMatrix as DM, DVector as DV, Vector as V};
use nalgebra::base::{dimension::Dim, storage::Storage};

use rand::prelude::*;
use rand::distributions::StandardNormal;

use fern;
use log::*;

pub use self::ff::*;
pub mod ff;

pub use self::utils::*;
pub mod utils;

pub fn cma(objective_function: impl VectorFn, N: f64, lambda: f64, stopeval: i64) {
    let mut xmean: DV<f64> = DV::new_random(N as usize)*10.;
    let mut sigma = 0.5;
    let stopfitness = 1. / 10_f64.powi(10);

    let mu = (lambda / 2.).floor();

    let weights = DV::from_fn(mu as usize, |_, _| (mu + 1.0).ln())
        - DV::from_fn(mu as usize, |i, _| ((i + 1) as f64).ln());
    let weights = &weights / sum(&weights);

    let mueff = sum(&weights).powi(2) / sum(&weights.map(|x| x * x));

    let cc = 4. / (N + 4.);
    let cs = (mueff + 2.) / (N + mueff + 3.);
    let mucov = mueff;
    let ccov = (1. / mucov) * 2. / (N + 1.4).powi(2)
        + (1. - 1. / mucov) * ((2. * mueff - 1.) / ((N + 2.).powi(2) + 2. * mueff));
    let damps = 1. + 2. * (((mueff - 1.) / (N + 1.)).sqrt() - 1.).max(0.) + cs;

    let mut pc = DV::<f64>::zeros(N as usize);
    let mut ps = DV::<f64>::zeros(N as usize);

    let mut B = DM::<f64>::identity(N as usize, N as usize);
    let mut D = DM::<f64>::identity(N as usize, N as usize);
    let mut C = &B * &D * (&B * &D).transpose();

    let mut eigeneval = 0;
    let chiN = N.powf(0.5) * (1. - 1. / (4. * N) + 1. / (21. * N.powi(2)));

    let mut counteval = 0;

    let mut arz: DM<f64> = DM::zeros(N as usize, lambda as usize);
    let mut arx: DM<f64> = DM::zeros(N as usize, lambda as usize);

    let mut zmean: DV<f64> = DV::zeros(mu as usize);

    let mut arfitness: DV<f64> = DV::zeros(lambda as usize);

    'outer: while counteval < stopeval {
        arz = DM::<f64>::from_distribution(N as usize, lambda as usize, &StandardNormal, &mut thread_rng());

        // Set new x candidates
        for k in 0..(lambda as usize) {
            arx.set_column(k, &(&xmean + sigma * (&B * &D * arz.column(k))));
        }

        arfitness = DV::<f64>::from_fn(lambda as usize, |k, _| {
            objective_function.apply(&arx.column(k))
        });
        counteval += lambda as i64;

        // Sort function outputs and inputs
        let perm_sort = quicksort_permutation(&arfitness);

        arfitness = &perm_sort * &arfitness;
        arx = &arx * &perm_sort.transpose();
        arz = &arz * &perm_sort.transpose();

        // Weighted recombination of the new candidates, according to the order of their images
        xmean = &arx.columns(0, mu as usize) * &weights;
        zmean = &arz.columns(0, mu as usize) * &weights;

        ps = (1. - cs) * ps + (cs * (2. - cs) * mueff).sqrt() * (&B * &zmean);

        let hsig =
            if ps.norm() / (1. - (1. - cs).powf(2. * counteval as f64 / lambda)).sqrt() / chiN
                < 1.5 + 1. / (N - 0.5)
            {
                1.
            } else {
                0.
            };

        pc = (1. - cc) * &pc + (cc * (2. - cc) * mueff).sqrt() * (&B * &D * &zmean);

        C = (1. - ccov) * &C
            + ccov * 1. / mucov * (&pc * &pc.transpose() + (1. - hsig) * cc * (2. - cc) * &C)
            + ccov
                * (1. - 1. / mucov)
                * (&B * &D * &arz.columns(0, mu as usize))
                * DM::from_diagonal(&weights)
                * (&B * &D * &arz.columns(0, mu as usize)).transpose();

        sigma = sigma * f64::exp((cs / damps) * (ps.norm() / chiN - 1.));

        if (counteval - eigeneval) as f64 > lambda / ccov / N / 10. {
            eigeneval = counteval;
            C = C.upper_triangle() + C.upper_triangle().transpose()
                - (DM::from_diagonal(&C.diagonal()));

            use nalgebra::linalg::SymmetricEigen;
            let mut Cp = DM::<f64>::zeros(N as usize, N as usize);
            Cp.copy_from(&C);
            let eigen = SymmetricEigen::new(Cp);
            D = DM::from_diagonal(&eigen.eigenvalues.map(f64::sqrt));
            B = eigen.eigenvectors;
        }

        if arfitness[0] < stopfitness {
            info!("arfitness[0] < stopfitness");
            break 'outer;
        }

        if arfitness[0]
            == arfitness[(1. + (lambda / 2.).floor()).min(2. + (lambda / 4.).ceil()) as usize]
        {
            sigma = sigma * f64::exp(0.2 + cs / damps);
            info!("escape flat fitness");
        }
    }

    // arx_s.column(0)
    info!("xmin: {:8.4}", arx.column(0));
}
