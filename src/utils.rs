use nalgebra::base::{DVector, Vector, DMatrix, Matrix};
// Returns a permutation matrix that sorts v
// FIXME: directly build the permutation matrix in the quick sort swapping columns?
// TODO: pass compare function
// usage: if P = quicksort_permutation(v), P*v sorts v;
// if v = f(M) each component of v is function of a column of M, M*P.transpose sorts the columns in M
// according to their image as input to f
pub fn quicksort_permutation(v: &DVector<f64>) -> DMatrix<f64> {
    fn quicksort(v: &DVector<f64>, mut perm: &mut DVector<usize>, left: usize, right: usize) -> () {
        if left < right {
            let p = partition(v, &mut perm, left, right);
            if p > 0 {
                quicksort(v, &mut perm, left, p - 1);
            }
            quicksort(v, &mut perm, p + 1, right);
        }
    }

    fn partition(v: &DVector<f64>, mut perm: &mut DVector<usize>, left: usize, right: usize) -> usize {
        let pivot = v[perm[right]];
        let mut i = left;

        for j in left..right {
            if v[perm[j]] < pivot {
                if i != j {
                    perm.swap_rows(i, j);
                }
                i += 1;
            }
        }
        perm.swap_rows(i, right);
        i
    }

    let n = v.nrows();
    let mut perm = DVector::<usize>::from_fn(n, |i, _| i);
    quicksort(v, &mut perm, 0, n - 1);

    fn permutation_matrix(p: &DVector<usize>) -> DMatrix<f64> {
        let n = p.nrows();
        let m = DMatrix::from_fn(n, n, |i, j| if j == p[i] { 1. } else { 0. });
        m
    }

    permutation_matrix(&perm)
}

pub fn sum(v: &DVector<f64>) -> f64 {
    v.iter().fold(0., |b, x| b + x)
}

// fn logplot2Dp(m: &DMatrix<f64>, name: i64) {
//     let mut xLOG = Vec::<f64>::new();
//     let mut yLOG = Vec::<f64>::new();

//     xLOG.push(0.);
//     yLOG.push(0.);
//     m.row(0).iter().for_each(|x| xLOG.push(*x));
//     m.row(1).iter().for_each(|y| yLOG.push(*y));
//     xLOG.push(2.);
//     yLOG.push(2.);
//     let lp = scatter_plot::<f64, f64>(xLOG, yLOG, None);
//     let mut figure = Figure::new();
//      figure.add_plot(lp.clone());
//     info!("Plotting {}", name);
//     print!(
//         "{:?}",
//         figure.save(&format!("./plots/{:04}.png", name))
//     );
// }

pub fn logplot2D(x: &DVector<f64>, y: &DVector<f64>, name: String) {
    let xstr = format!(
        "c({})",
        x.iter().map(f64::to_string).collect::<Vec<_>>().join(",")
    );
    let ystr = format!(
        "c({})",
        y.iter().map(f64::to_string).collect::<Vec<_>>().join(",")
    );

    use std::process::Command;
    let go = Command::new("/home/nbrr/.nix-profile/bin/Rscript")
        .arg("plot2D.R")
        .arg(xstr)
        .arg(ystr)
        .arg(format!("{}.png", name))
        .output()
        .expect("failed to execute process");

    println!("{:?}", go.stdout);
}

pub fn logplot3D(x: &DVector<f64>, y: &DVector<f64>, z: &DVector<f64>, name: String) {
    let xstr = format!(
        "c({})",
        x.iter().map(f64::to_string).collect::<Vec<_>>().join(",")
    );
    let ystr = format!(
        "c({})",
        y.iter().map(f64::to_string).collect::<Vec<_>>().join(",")
    );
    let zstr = format!(
        "c({})",
        z.iter().map(f64::to_string).collect::<Vec<_>>().join(",")
    );

    use std::process::Command;
    let go = Command::new("/home/nbrr/.nix-profile/bin/Rscript")
        .arg("plot3D.R")
        .arg(xstr)
        .arg(ystr)
        .arg(zstr)
        .arg(format!("{}.png", name))
        .output()
        .expect("failed to execute process");

    println!("{:?}", go.stdout);
}
